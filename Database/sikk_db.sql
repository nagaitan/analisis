/*
SQLyog Community v12.2.4 (64 bit)
MySQL - 5.6.21 : Database - cis
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cis` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `cis`;

/*Table structure for table `adak_kelas` */

DROP TABLE IF EXISTS `adak_kelas`;

CREATE TABLE `adak_kelas` (
  `kelas_id` int(11) NOT NULL AUTO_INCREMENT,
  `ta` int(4) NOT NULL DEFAULT '0',
  `nama` varchar(20) NOT NULL DEFAULT '',
  `ket` text,
  `dosen_wali_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`kelas_id`),
  KEY `FK_adak_kelas_wali` (`dosen_wali_id`),
  CONSTRAINT `FK_adak_kelas_wali` FOREIGN KEY (`dosen_wali_id`) REFERENCES `hrdx_dosen` (`dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `adak_kelas` */

/*Table structure for table `adak_registrasi` */

DROP TABLE IF EXISTS `adak_registrasi`;

CREATE TABLE `adak_registrasi` (
  `registrasi_id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `status_akhir_registrasi` varchar(50) DEFAULT 'Aktif',
  `ta` varchar(30) NOT NULL DEFAULT '0',
  `sem_ta` int(11) NOT NULL DEFAULT '0',
  `sem` smallint(6) NOT NULL DEFAULT '0',
  `tgl_daftar` date DEFAULT NULL,
  `keuangan` double DEFAULT NULL,
  `kelas` varchar(20) DEFAULT NULL,
  `id` varchar(20) DEFAULT NULL,
  `nr` float DEFAULT NULL,
  `koa_approval` int(11) NOT NULL DEFAULT '0',
  `koa_approval_bp` int(11) NOT NULL DEFAULT '0',
  `kelas_id` int(11) DEFAULT NULL,
  `dosen_wali_id` int(11) DEFAULT NULL,
  `keasramaan_id` int(11) DEFAULT NULL,
  `dim_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`registrasi_id`),
  KEY `fk_t_registrasi_t_kelas1_idx` (`kelas_id`),
  KEY `fk_t_registrasi_t_profile1_idx` (`dosen_wali_id`),
  KEY `fk_t_registrasi_t_dim1_idx` (`dim_id`),
  KEY `keasramaan_id` (`keasramaan_id`),
  CONSTRAINT `FK_adak_registrasi_dosen_wali` FOREIGN KEY (`dosen_wali_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_prkl_registrasi_kelas` FOREIGN KEY (`kelas_id`) REFERENCES `adak_kelas` (`kelas_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `adak_registrasi_ibfk_1` FOREIGN KEY (`keasramaan_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_t_registrasi_t_dim1` FOREIGN KEY (`dim_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `adak_registrasi` */

/*Table structure for table `dimx_dim` */

DROP TABLE IF EXISTS `dimx_dim`;

CREATE TABLE `dimx_dim` (
  `dim_id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` varchar(8) NOT NULL DEFAULT '',
  `no_usm` varchar(15) NOT NULL DEFAULT '',
  `jalur` varchar(20) DEFAULT NULL,
  `user_name` varchar(10) DEFAULT NULL,
  `kbk_id` varchar(20) DEFAULT NULL,
  `ref_kbk_id` int(11) DEFAULT NULL,
  `kpt_prodi` varchar(10) DEFAULT NULL,
  `id_kur` int(4) NOT NULL DEFAULT '0',
  `tahun_kurikulum_id` int(11) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `gol_darah` char(2) DEFAULT NULL,
  `golongan_darah_id` int(11) DEFAULT NULL,
  `jenis_kelamin` char(1) DEFAULT NULL,
  `jenis_kelamin_id` int(11) DEFAULT NULL,
  `agama` varchar(30) DEFAULT NULL,
  `agama_id` int(11) DEFAULT NULL,
  `alamat` text,
  `kabupaten` varchar(50) DEFAULT NULL,
  `kode_pos` varchar(5) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `hp` varchar(50) DEFAULT NULL,
  `hp2` varchar(50) DEFAULT NULL,
  `no_ijazah_sma` varchar(100) DEFAULT NULL,
  `nama_sma` varchar(50) DEFAULT NULL,
  `asal_sekolah_id` int(11) DEFAULT NULL,
  `alamat_sma` text,
  `kabupaten_sma` varchar(100) DEFAULT NULL,
  `telepon_sma` varchar(50) DEFAULT NULL,
  `kodepos_sma` varchar(8) DEFAULT NULL,
  `thn_masuk` int(11) DEFAULT NULL,
  `status_akhir` varchar(50) DEFAULT 'Aktif',
  `nama_ayah` varchar(50) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `no_hp_ayah` varchar(50) DEFAULT NULL,
  `no_hp_ibu` varchar(50) DEFAULT NULL,
  `alamat_orangtua` text,
  `pekerjaan_ayah` varchar(100) DEFAULT NULL,
  `pekerjaan_ayah_id` int(11) DEFAULT NULL,
  `keterangan_pekerjaan_ayah` text,
  `penghasilan_ayah` varchar(50) DEFAULT NULL,
  `penghasilan_ayah_id` int(11) DEFAULT NULL,
  `pekerjaan_ibu` varchar(100) DEFAULT NULL,
  `pekerjaan_ibu_id` int(11) DEFAULT NULL,
  `keterangan_pekerjaan_ibu` text,
  `penghasilan_ibu` varchar(50) DEFAULT NULL,
  `penghasilan_ibu_id` int(11) DEFAULT NULL,
  `nama_wali` varchar(50) DEFAULT NULL,
  `pekerjaan_wali` varchar(50) DEFAULT NULL,
  `pekerjaan_wali_id` int(11) DEFAULT NULL,
  `keterangan_pekerjaan_wali` text,
  `penghasilan_wali` varchar(50) DEFAULT NULL,
  `penghasilan_wali_id` int(11) DEFAULT NULL,
  `alamat_wali` text,
  `telepon_wali` varchar(20) DEFAULT NULL,
  `no_hp_wali` varchar(50) DEFAULT NULL,
  `pendapatan` varchar(50) DEFAULT NULL,
  `ipk` float DEFAULT '0',
  `anak_ke` tinyint(4) DEFAULT NULL,
  `dari_jlh_anak` tinyint(4) DEFAULT NULL,
  `jumlah_tanggungan` tinyint(4) DEFAULT NULL,
  `nilai_usm` float DEFAULT NULL,
  `score_iq` tinyint(4) DEFAULT NULL,
  `rekomendasi_psikotest` varchar(4) DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `kode_foto` varchar(100) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`dim_id`),
  UNIQUE KEY `NIM_UNIQUE` (`nim`),
  KEY `NIM` (`nim`),
  KEY `FK_dimx_dim_thn_krkm` (`tahun_kurikulum_id`),
  KEY `FK_dimx_dim_user` (`user_id`),
  KEY `FK_dimx_dim_ref_kbk` (`ref_kbk_id`),
  KEY `FK_dimx_dim_asal_sekolah` (`asal_sekolah_id`),
  KEY `FK_dimx_dim_golongan_darah` (`golongan_darah_id`),
  KEY `FK_dimx_dim_jenis_kelamin` (`jenis_kelamin_id`),
  KEY `FK_dimx_dim_agama` (`agama_id`),
  KEY `FK_dimx_dim_pekerjaan_ayah` (`pekerjaan_ayah_id`),
  KEY `FK_dimx_dim_penghasilan_ayah` (`penghasilan_ayah_id`),
  KEY `FK_dimx_dim_pekerjaan_ibu` (`pekerjaan_ibu_id`),
  KEY `FK_dimx_dim_penghasilan_ibu` (`penghasilan_ibu_id`),
  KEY `FK_dimx_dim_pekerjaan_wali` (`pekerjaan_wali_id`),
  KEY `FK_dimx_dim_penghasilan_wali_id` (`penghasilan_wali_id`),
  CONSTRAINT `FK_dimx_dim_agama` FOREIGN KEY (`agama_id`) REFERENCES `mref_r_agama` (`agama_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_asal_sekolah` FOREIGN KEY (`asal_sekolah_id`) REFERENCES `mref_r_asal_sekolah` (`asal_sekolah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_golongan_darah` FOREIGN KEY (`golongan_darah_id`) REFERENCES `mref_r_golongan_darah` (`golongan_darah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_jenis_kelamin` FOREIGN KEY (`jenis_kelamin_id`) REFERENCES `mref_r_jenis_kelamin` (`jenis_kelamin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_pekerjaan_ayah` FOREIGN KEY (`pekerjaan_ayah_id`) REFERENCES `mref_r_pekerjaan` (`pekerjaan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_pekerjaan_ibu` FOREIGN KEY (`pekerjaan_ibu_id`) REFERENCES `mref_r_pekerjaan` (`pekerjaan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_pekerjaan_wali` FOREIGN KEY (`pekerjaan_wali_id`) REFERENCES `mref_r_pekerjaan` (`pekerjaan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_penghasilan_ayah` FOREIGN KEY (`penghasilan_ayah_id`) REFERENCES `mref_r_penghasilan` (`penghasilan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_penghasilan_ibu` FOREIGN KEY (`penghasilan_ibu_id`) REFERENCES `mref_r_penghasilan` (`penghasilan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_penghasilan_wali_id` FOREIGN KEY (`penghasilan_wali_id`) REFERENCES `mref_r_penghasilan` (`penghasilan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_ref_kbk` FOREIGN KEY (`ref_kbk_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_thn_krkm` FOREIGN KEY (`tahun_kurikulum_id`) REFERENCES `krkm_r_tahun_kurikulum` (`tahun_kurikulum_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_dimx_dim_user` FOREIGN KEY (`user_id`) REFERENCES `sysx_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dimx_dim` */

/*Table structure for table `hrdx_dosen` */

DROP TABLE IF EXISTS `hrdx_dosen`;

CREATE TABLE `hrdx_dosen` (
  `dosen_id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai_id` int(11) DEFAULT NULL,
  `nidn` varchar(10) DEFAULT NULL,
  `prodi_id` int(11) DEFAULT NULL,
  `golongan_kepangkatan_id` int(11) DEFAULT NULL,
  `jabatan_akademik_id` int(11) DEFAULT NULL,
  `status_ikatan_kerja_dosen_id` int(11) DEFAULT NULL,
  `gbk_1` int(11) DEFAULT NULL,
  `gbk_2` int(11) DEFAULT NULL,
  `aktif_start` date DEFAULT '0000-00-00',
  `aktif_end` date DEFAULT '0000-00-00',
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `temp_id_old` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`dosen_id`),
  KEY `FK_hrdx_dosen` (`golongan_kepangkatan_id`),
  KEY `FK_hrdx_dosen_jab` (`jabatan_akademik_id`),
  KEY `FK_hrdx_dosen_stts` (`status_ikatan_kerja_dosen_id`),
  KEY `FK_hrdx_dosen_gbk` (`gbk_1`),
  KEY `FK_hrdx_dosen_pegawai` (`pegawai_id`),
  KEY `FK_hrdx_dosen_gbk2` (`gbk_2`),
  KEY `FK_hrdx_dosen_prodi` (`prodi_id`),
  CONSTRAINT `FK_hrdx_dosen` FOREIGN KEY (`golongan_kepangkatan_id`) REFERENCES `mref_r_golongan_kepangkatan` (`golongan_kepangkatan_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_gbk` FOREIGN KEY (`gbk_1`) REFERENCES `mref_r_gbk` (`gbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_gbk2` FOREIGN KEY (`gbk_2`) REFERENCES `mref_r_gbk` (`gbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_jab` FOREIGN KEY (`jabatan_akademik_id`) REFERENCES `mref_r_jabatan_akademik` (`jabatan_akademik_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_pegawai` FOREIGN KEY (`pegawai_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_prodi` FOREIGN KEY (`prodi_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_dosen_stts` FOREIGN KEY (`status_ikatan_kerja_dosen_id`) REFERENCES `mref_r_status_ikatan_kerja_dosen` (`status_ikatan_kerja_dosen_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_dosen` */

/*Table structure for table `hrdx_pegawai` */

DROP TABLE IF EXISTS `hrdx_pegawai`;

CREATE TABLE `hrdx_pegawai` (
  `pegawai_id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_old_id` varchar(20) DEFAULT NULL,
  `nama` varchar(135) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `nip` varchar(45) DEFAULT NULL,
  `kpt_no` varchar(10) DEFAULT NULL,
  `kbk_id` varchar(20) DEFAULT NULL,
  `ref_kbk_id` int(11) DEFAULT NULL,
  `alias` varchar(9) DEFAULT NULL,
  `posisi` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(60) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `agama_id` int(11) DEFAULT NULL,
  `jenis_kelamin_id` int(11) DEFAULT NULL,
  `golongan_darah_id` int(11) DEFAULT NULL,
  `hp` varchar(20) DEFAULT NULL,
  `telepon` varchar(45) DEFAULT NULL,
  `alamat` blob,
  `alamat_libur` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(150) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `kabupaten_id` int(11) DEFAULT NULL,
  `kode_pos` varchar(15) DEFAULT NULL,
  `no_ktp` varchar(255) DEFAULT NULL,
  `email` text,
  `ext_num` char(3) DEFAULT NULL,
  `study_area_1` varchar(50) DEFAULT NULL,
  `study_area_2` varchar(50) DEFAULT NULL,
  `jabatan` char(1) DEFAULT NULL,
  `jabatan_akademik_id` int(11) DEFAULT NULL,
  `gbk_1` int(11) DEFAULT NULL,
  `gbk_2` int(11) DEFAULT NULL,
  `status_ikatan_kerja_pegawai_id` int(11) DEFAULT NULL,
  `status_akhir` char(1) DEFAULT NULL,
  `status_aktif_pegawai_id` int(11) DEFAULT NULL,
  `tanggal_masuk` date DEFAULT '0000-00-00',
  `tanggal_keluar` date DEFAULT '0000-00-00',
  `nama_bapak` varchar(50) DEFAULT NULL,
  `nama_ibu` varchar(50) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `status_marital_id` int(11) DEFAULT NULL,
  `nama_p` varchar(50) DEFAULT NULL,
  `tgl_lahir_p` date DEFAULT NULL,
  `tmp_lahir_p` varchar(50) DEFAULT NULL,
  `pekerjaan_ortu` varchar(100) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(32) DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`pegawai_id`),
  KEY `FK_hrdx_pegawai_JK` (`jenis_kelamin_id`),
  KEY `FK_hrdx_pegawai_agama` (`agama_id`),
  KEY `FK_hrdx_pegawai_golda` (`golongan_darah_id`),
  KEY `FK_hrdx_pegawai_kab` (`kabupaten_id`),
  KEY `FK_hrdx_pegawai_sts_aktf` (`status_aktif_pegawai_id`),
  KEY `FK_hrdx_pegawai_sts_iktn` (`status_ikatan_kerja_pegawai_id`),
  KEY `FK_hrdx_pegawai_sts_martl` (`status_marital_id`),
  KEY `FK_hrdx_pegawai_user` (`user_id`),
  KEY `FK_hrdx_pegawai_jabatan_akademik` (`jabatan_akademik_id`),
  KEY `FK_hrdx_pegawai_kbk` (`ref_kbk_id`),
  CONSTRAINT `FK_hrdx_pegawai_JK` FOREIGN KEY (`jenis_kelamin_id`) REFERENCES `mref_r_jenis_kelamin` (`jenis_kelamin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_agama` FOREIGN KEY (`agama_id`) REFERENCES `mref_r_agama` (`agama_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_golda` FOREIGN KEY (`golongan_darah_id`) REFERENCES `mref_r_golongan_darah` (`golongan_darah_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_jabatan_akademik` FOREIGN KEY (`jabatan_akademik_id`) REFERENCES `mref_r_jabatan_akademik` (`jabatan_akademik_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_kab` FOREIGN KEY (`kabupaten_id`) REFERENCES `mref_r_kabupaten` (`kabupaten_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_kbk` FOREIGN KEY (`ref_kbk_id`) REFERENCES `inst_prodi` (`ref_kbk_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_sts_aktf` FOREIGN KEY (`status_aktif_pegawai_id`) REFERENCES `mref_r_status_aktif_pegawai` (`status_aktif_pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_sts_iktn` FOREIGN KEY (`status_ikatan_kerja_pegawai_id`) REFERENCES `mref_r_status_ikatan_kerja_pegawai` (`status_ikatan_kerja_pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_hrdx_pegawai_sts_martl` FOREIGN KEY (`status_marital_id`) REFERENCES `mref_r_status_marital` (`status_marital_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hrdx_pegawai` */

/*Table structure for table `sikk_akun` */

DROP TABLE IF EXISTS `sikk_akun`;

CREATE TABLE `sikk_akun` (
  `akun_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` char(30) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`akun_id`),
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `sikk_akun_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sikk_akun_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sikk_akun_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `sikk_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sikk_akun` */

/*Table structure for table `sikk_approval` */

DROP TABLE IF EXISTS `sikk_approval`;

CREATE TABLE `sikk_approval` (
  `approval_id` int(11) NOT NULL AUTO_INCREMENT,
  `izin_keluar_id` int(11) DEFAULT NULL,
  `approver_id` int(11) DEFAULT NULL,
  `status_approval` tinyint(1) DEFAULT NULL,
  `alasan` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`approval_id`),
  KEY `izin_keluar_id` (`izin_keluar_id`),
  KEY `sikk_approval_ibfk_2` (`approver_id`),
  CONSTRAINT `sikk_approval_ibfk_1` FOREIGN KEY (`izin_keluar_id`) REFERENCES `sikk_izin_keluar` (`izin_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sikk_approval_ibfk_2` FOREIGN KEY (`approver_id`) REFERENCES `hrdx_pegawai` (`pegawai_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sikk_approval` */

/*Table structure for table `sikk_izin_keluar` */

DROP TABLE IF EXISTS `sikk_izin_keluar`;

CREATE TABLE `sikk_izin_keluar` (
  `izin_id` int(11) NOT NULL,
  `mhs_id` int(11) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `time_start` datetime DEFAULT NULL,
  `time_end` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`izin_id`),
  KEY `mhs_id` (`mhs_id`),
  CONSTRAINT `sikk_izin_keluar_ibfk_9` FOREIGN KEY (`mhs_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sikk_izin_keluar` */

/*Table structure for table `sikk_log` */

DROP TABLE IF EXISTS `sikk_log`;

CREATE TABLE `sikk_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `mhs_id` int(11) DEFAULT NULL,
  `time_in` time DEFAULT NULL,
  `time_out` datetime DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `mhs_id` (`mhs_id`),
  CONSTRAINT `sikk_log_ibfk_1` FOREIGN KEY (`mhs_id`) REFERENCES `dimx_dim` (`dim_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sikk_log` */

/*Table structure for table `sikk_role` */

DROP TABLE IF EXISTS `sikk_role`;

CREATE TABLE `sikk_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(32) DEFAULT NULL,
  `desc` varchar(50) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(32) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sikk_role` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
